# Module description

This module provides a new drush command to call any other existent drush
command only once, when some specific version is specified.

## Mode of use:

  drush run:once id version drushCommandToExecute

Check drush help run:once for all the options.

## Use case:

- Importing translations with locale:import is a slow process.
- If the source PO files is not modified it does not makes sense ti run the
import process.
- You could use then drush run:once in the following way:

drush run:once importSpanishStrings v0.1 locale:import --args='es,path/to/es.po' --options='override=all,type=customize

Where:

- `importSpanishStrings` is the name you give to the action running the locale:import command.
- `v0.1` is the version run:once will check in the database to see if this command need to be run.
- `locale:import --args='es,path/to/es.po' --options='override=all,type=customize` is the actual drush command to execute.

If you attempt to run the same command twice, nothing will happen since the
version `v0.1` for `importSpanishStrings` is already set in the database.

The version specified can be anything. You could use commit hashes to compare
against. Or some sort of MD5 or SHA1 hash against some specific file.

### Other commands:

- `run:once:info` will return all the keys and values assigned in the system.
