<?php

namespace Drush\Commands\drush_run_once;

use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drush\Commands\DrushCommands;
use Drush\Drush;
use Drush\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\OutputFormatters\StructuredData\PropertyList;

/**
 * A Drush command that is only executed once until the specified version changes.
 */
class DrushRunOnceCommands extends DrushCommands implements SiteAliasManagerAwareInterface {

  use SiteAliasManagerAwareTrait;

  const DRUSH_RUN_ONCE_PREFIX = 'drush_run_once_';

  /**
   * Runs a drush command just once and register the last version used to run this command.
   *
   * @param $id
   *   And identifier of the task to run.
   * @param $version
   *   A version to check before running the command. Can be any string.
   * @param $drush_command
   *   A command to execute if the conditions are not meet.
   * @option args
   *   A space separated list of arguments to pass to the drush command.
   * @option options
   *   A space separated list of options to pass to the drush command.
   * @usage drush run:once run_cron 1.0.0 cron
   *   Will execute the `cron` command only if there is no reference to version
   *   1.0.0 in the log of executed commands for the key `run_cron`.
   * @usage run:once syncSpanish v1.0 locale:import --args='es,path/to/es.po' --options='override=all,type=customized'
   *   Will run locale:import es path/to/es.po --override=all --type=customized
   *   unless syncSpanish is already set to v.10 in the Drupal state API.
   * @command run:once
   * @bootstrap full
   */
  public function runOnce($id, $version, $drush_command, $options = ['args' => '', 'options' => '']) {
    $key = self::DRUSH_RUN_ONCE_PREFIX . $id;
    $message_args = ['@command' => $id, '@version' => $version];
    $last_version = \Drupal::state()->get($key);
    if (!empty($last_version) && $last_version == $version) {
      $this->logger()->warning(dt('Command: @command is already in version @version. Skipping.', $message_args));
      return;
    }

    $drush_args = $this->extractArgs($options);
    $drush_options = $this->extractOptions($options);

    $process = Drush::drush(Drush::aliasManager()->getSelf(), $drush_command, $drush_args, $drush_options);
    // Print the output of the executed command.
    $process->mustRun(function ($type, $buffer) { echo $buffer; });

    \Drupal::state()->set($key, $version);
    $this->logger()->success(dt('Command: @command ran successfully. Version set to @version.', $message_args));
  }

  /**
   * Returns information about all the operations executed as run:once.
   *
   * @command run:once:info
   * @bootstrap full
   */
  public function info($options = ['format' => 'json']) {
    $all_states = \Drupal::service('keyvalue')->get('state')->getAll();
    $relevant = array_filter($all_states, function ($key) {
      return strpos($key, self::DRUSH_RUN_ONCE_PREFIX) === 0;
    }, ARRAY_FILTER_USE_KEY);

    $data = [];
    $len = strlen(self::DRUSH_RUN_ONCE_PREFIX);
    foreach ($relevant as $key => $value) {
      $data[substr($key, $len)] = $value;
    }
    return new PropertyList($data);
  }

  /**
   * Extracts arguments from --args to pass to the drush command.
   *
   * @param array $options
   *   An array of options.
   *
   * @return array
   *   The arguments to pass to drush_command.
   */
  protected function extractArgs(array $options) {
    if (empty($options['args'])) {
      return [];
    }
    return explode(',', $options['args']);
  }

  /**
   * Extracts options from --options to pass to the drush command.
   *
   * @param array $options
   *   An array of options.
   *
   * @return array
   *   The options to pass to drush_command.
   */
  protected function extractOptions(array $options) {
    if (empty($options['options'])) {
      return [];
    }
    $drush_options = [];
    foreach (explode(',', $options['options']) as $option) {
      if (strpos($option, '=') === FALSE) {
        continue;
      }
      list($option_key, $option_value) = explode('=', $option);
      $drush_options[$option_key] = $option_value;
    }
    return $drush_options;
  }

}
